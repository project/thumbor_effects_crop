<?php

namespace Drupal\thumbor_effects_crop\Exception;

/**
 * Exception thrown when applying an invalid crop effect.
 */
class InvalidCropEffectException extends \Exception {}
